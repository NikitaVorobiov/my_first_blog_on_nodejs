var express = require( 'express' ),
    bodyParser = require( 'body-parser' ),
    app = express();

app.use( bodyParser.urlencoded( { extended: true} ) );
app.use( bodyParser.json() );

var posts = [
    {id: 1, title : "First post in my blog", text : "It's my first post on my blog whitch i did with using nodejs and express. Sorry but i don't use SQL" },
    {id: 2,title : "API Reference Documentation", text : "The API reference documentation provides detailed information about a function or object in Node.js. This documentation indicates what arguments a method accepts, the return value of that method, and what errors may be related to that method. It also indicates which methods are available for different versions of Node.js." },
    {id: 3,title : "ES6 Features", text : "The ES6 section describes the three ES6 feature groups, and details which features are enabled by default in Node.js, alongside explanatory links. It also shows how to find which version of V8 shipped with a particular Node.js release." }
];


app.get('/posts', function ( req, res ) {
    res.send(posts);
} );

app.get('/', function (req, res) {
   res.render('index.ejs'); 
});

app.get( '/post/:id', function ( req, res ) {
    var id = req.params.id;
    res.render( 'post.ejs', { post: posts[ id - 1 ] } );
} );

app.get( '/write', function ( req, res ) {
    res.render( 'write.ejs' );
} );

app.post('/write', function (req, res) {
    var title = req.body.title,
        text = req.body.text;

    posts.push( { title: title, text: text } );

    res.redirect( '/' );
})

app.listen(3000, function () {
    console.log("3k port");
});