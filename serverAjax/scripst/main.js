
var classXhr = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;

var xhr = new classXhr();


function getAllPosts() {
    xhr.open('GET', '/posts', false);
    xhr.send();

    if (xhr.status != 200) {
        console.log(xhr.status + " : " + xhr.statusText);
    } else {
        var obj = JSON.parse(xhr.response);
        var html = tplInHtml(obj, tpl);

        document.getElementById('info').innerHTML = html;
    }
}

function getPostById() {

}

function updatePost() {

}

function deletePost() {
    
}

var tpl = `
    <div>
        <span class="id">
            {{id}}
        </span>
        <span class="title">
            {{title}}
        </span>
        <span class="text">
            {{text}}
        </span>
    </div>
`;

function tplInHtml(list, tpl) {
    var rezult = ``;

    for (var i = 0; i < list.length; i++) {
        var begin = tpl;

        while (begin.indexOf(`{{`) != -1) {
            rezult += begin.slice(0, begin.indexOf(`{{`));
            rezult += list[i][ begin.slice(begin.indexOf(`{{`) + 2, begin.indexOf(`}}`)) ];
            begin = begin.slice( begin.indexOf(`}}`) + 2, begin.length );

            if (begin.indexOf(`{{`) == -1) {
                rezult += begin;
            }
        }
    }
    return rezult;
}